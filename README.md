Should be occasionally rebased on https://src.fedoraproject.org/rpms/generic-release

```bash
git remote add upstream https://src.fedoraproject.org/rpms/generic-release

git fetch upstream

git rebase upstream/master
```
